/* Class for COMP2931 Coursework 1 */

package comp2931.cwk1;


import java.util.ArrayList;
import java.util.Calendar;

/**
 * Simple representation of a date.
 */
  public class Date {

  private int year;
  private int month;
  private int day;

  public Date() {
    Calendar now = Calendar.getInstance();
    year = now.get(Calendar.DAY_OF_YEAR);
    month = now.get(Calendar.DAY_OF_MONTH);
  }
  /**
   * Creates a date using the given values for year, month and day.
   *
   * @param y Year
   * @param m Month
   * @param d Day
   */
  public Date(int y, int m, int d) {
    set(y, m, d);
  }
  /* dateString string containing year, month and day
   * IllegalArgumentException if year, month or day are invalid
   */
  public Date(String dateString) {
    String[] parts = dateString.split("-");
    if (parts.length < 2 || parts.length > 3) {
      throw new IllegalArgumentException("ill-formed time string");
    }

    int y = Integer.parseInt(parts[0]);
    int m = Integer.parseInt(parts[1]);
    int d = Integer.parseInt(parts[2]);

    set(y, m, d);
  }

  /**
   * Returns the year component of this date.
   *
   * @return Year
   */
  public int getYear() {
    return year;
  }

  /**
   * Returns the month component of this date.
   *
   * @return Month
   */
  public int getMonth() {
    return month;
  }

  /**
   * Returns the day component of this date.
   *
   * @return Day
   */
  public int getDay() {
    return day;
  }

  public int getDayOfYear(){
    int leapyear;
    int theMonth = 0, i;
    if((year % 4 == 0 && year % 100 !=0)|| year % 400 == 0){
       leapyear = 29;
    }
    else
      {
       leapyear = 28;
    }

    ArrayList<Integer> monthDays= new ArrayList<Integer>();
    monthDays.add(0,31);
    monthDays.add(1,leapyear);
    monthDays.add(2,31);
    monthDays.add(3,30);
    monthDays.add(4,31);
    monthDays.add(5,30);
    monthDays.add(6,31);
    monthDays.add(7,31);
    monthDays.add(8,30);
    monthDays.add(9,31);
    monthDays.add(10,30);
    monthDays.add(11,31);
    i = month;


    for(int j = 0; j != i-1; j++){
      theMonth = theMonth + monthDays.get(j);
  }

    return theMonth + day;
  }

  /**
   * Provides a string representation of this date.
   *
   * ISO 8601 format is used (YYYY-MM-DD).
   *
   * @return Date as a string
   */
  @Override
  public String toString() {
    return String.format("%04d-%02d-%2d", getYear(), getMonth(), getDay());
  }

  /**
   * Tests if this date is equal to another.
   *
   * The two objects are considered equal if both are instances of
   * the Date class and both represent exactly the same
   * date of year.
   *
   * @return true if this Date object is equal to the other, false otherwise
   */
  @Override
  public boolean equals(Object other) {
    if (other == this) {
      // 'other' is same object as this one!
      return true;
    }
    else if (! (other instanceof Date)) {
      // 'other' is not a date object
      return false;
    }
    else {
      // Compare fields
      Date otherTime = (Date) other;
      return getYear() == otherTime.getYear()
          && getMonth() == otherTime.getMonth()
          && getDay() == otherTime.getDay();
    }
  }

  //define year, month and day.
  //make sure the date is valid
  private void set(int y, int m, int d) {
    if (y < 0 || y > 9999) {
      throw new IllegalArgumentException("year out of range");
    } else if (m < 0 || m > 12) {
      throw new IllegalArgumentException("month out of range");
    } else if (d < 0 || d > 31) {
      throw new IllegalArgumentException("day out of range");
    } else {
      year = y;
      month = m;
      day = d;
    }
  }
  public void getCalendar(){

    Calendar cal = Calendar.getInstance();


    int y = cal.get(Calendar.YEAR);

    int m = (cal.get(Calendar.MONTH)) + 1;

    int d = cal.get(Calendar.DAY_OF_MONTH);

  }

}
