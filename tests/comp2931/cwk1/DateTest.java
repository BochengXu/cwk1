package comp2931.cwk1;

//import com.sun.org.apache.xpath.internal.operations.String;
import org.junit.Before;
import org.junit.Test;
import java.lang.IllegalArgumentException;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class DateTest {

  @Test
  public void year(){
  Date TwentyFive = new Date(2005,00,00);
  Date TwentyTen = new Date(2010,00,00);
//  Date Mone = new Date(-1,00,00); //Test the year out of range

  assertThat(TwentyTen.getYear(), is(2010));
  assertThat(TwentyFive.getYear(), is(2005));
}

  @Test(expected=IllegalArgumentException.class)
  public void yearTooLow() {
    new Date(-1, 0, 0);
  }

  @Test(expected=IllegalArgumentException.class)
  public void monthTooLow() {
    new Date(-1, 0, 0);
  }

  @Test(expected=IllegalArgumentException.class)
  public void dayTooLow() {
    new Date(-1, 0, 0);
  }

  @Test(expected=IllegalArgumentException.class)
  public void yearTooHigh() {
    new Date(10000, 0, 0);
  }

  @Test(expected=IllegalArgumentException.class)
  public void monthTooHigh() {
    new Date(0000, 13, 0);
  }

  @Test(expected=IllegalArgumentException.class)
  public void dayTooHigh() {
    new Date(0000, 0, 32);
  }

  @Test
  public void month(){
  Date October = new Date(0000,10,00);
  Date May = new Date(0000,05,00);
//  Date Mone = new Date(0000,-01,00); //Test the month out of range

  assertThat(October.getMonth(), is(10));
  assertThat(May.getMonth(), is(05));
}

@Test
  public void day(){
  Date Tenth = new Date(0000,00,10);
  Date Fifth = new Date(0000,00,05);
//  Date Mone = new Date(0000,00,-1); //Test the month out of range

  assertThat(Tenth.getDay(), is(10));
  assertThat(Fifth.getDay(), is(05));
}

@Test
  public void dateToString() throws Exception {
  Date stringToDate = new Date(2017,10,17);
  assertEquals("2017-10-17", stringToDate.toString());
  }

@Test
  public void equality() {
  Date TwentyTenNewYear = new Date(2010, 01, 01);

  assertTrue(TwentyTenNewYear.equals(TwentyTenNewYear));
  assertTrue(TwentyTenNewYear.equals(new Date(2010, 01, 01)));
  assertFalse(TwentyTenNewYear.equals(new Date(2011, 01, 01)));
  assertFalse(TwentyTenNewYear.equals(new Date(2010, 00, 01)));
  assertFalse(TwentyTenNewYear.equals(new Date(2010, 01, 00)));
  assertFalse(TwentyTenNewYear.equals("2010-01-01"));
  }

  private Date TwentyTenNewYear;
@Before
  public void setUp() throws Exception {
  TwentyTenNewYear = new Date(2015, 01, 01);
  }

@Test
  public void dayOfYear() {
  Date twentyTenNewYearEve = new Date(2010, 10, 31);
  Date twoThousandNewYearEve = new Date(2000, 12, 31);

  assertThat(twentyTenNewYearEve.getDayOfYear(), is(304));
  assertThat(twoThousandNewYearEve.getDayOfYear(), is(366));
}

@Test
  public void calender(){
  Date a = new Date();
}


}